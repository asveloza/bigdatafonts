# -*- coding: utf-8 -*-
"""
@author: Al amanecer me llamo Khepri, al mediodía Ra y al atardecer Atum.
@autor: Fabian Fuertes
@Autor Allan Veloza
@Autor Arnulfo Garzon
@Autor NN

"""
try:
    import os, sys, inspect
    from pathlib import Path as p
    from pandas_profiling import ProfileReport

except Exception as exc:
            print('Module(s) {} are missing.:'.format(str(exc)))

cmd_folder_b = os.path.realpath(
    os.path.abspath(os.path.split(
        inspect.getfile( inspect.currentframe()))[0]))

cmd_folder = str(p(cmd_folder_b) /'clases')
if cmd_folder not in sys.path:sys.path.insert(0, cmd_folder)

from cls_carga_datos import carga_data_mf as data_loader

#%%

''' Crear una instancia de la clase '''
ld_c = data_loader()
ld_c.path = os.path.abspath(os.path.join('__file__', '..','..'))


#%%

'''Autenitcación en el api de Kaggle'''
ld_c.set_kaggle_api()
###valida acceso
print(ld_c.status)
if not ld_c.status:
    ld_c.control_error("No se pudo autenticar en kaggle",True)
    #%%
''' Existe el directorio destino '''
path_data = str(p(ld_c.path) / 'Dataset'/'kaggle')
ld_c.check_path(path_data)
if not ld_c.dir_exist:
    ld_c.control_error("No existe el directorio destino",True)
#%%
''' Espacio disponible en disco '''
#valida espacio disponible
if ld_c.check_free_space(ld_c.path) > ld_c.check_used_space(path_data):
    ld_c.control_error("No hay espacio en la ruta local",True)
#%%
'''Lista Datasets'''
ld_c.competition = "antaresnyc/metagenomics/tasks?taskId=2955"#"mkechinov/ecommerce-behavior-data-from-multi-category-store"
ld_c.path = path_data

ld_c.list_dataset_kaggle()
    
#%%
''' Listar archivos en una competencia'''

ld_c.datasets_list_files()
if len(ld_c.lst_files_d) == 0:
    ld_c.control_error("No hay archivos para descargar de kaggle",True)
#%%    
''' Descargar Dataset'''
ld_c.get_dataset_kaggle()


#%% 
   
''' Cargar listar archivos según el tipo'''
ld_c.get_lst_files(path_data,'zip')
print('{}Instancia kaggle:'.format(os.linesep))
ld_c.muestra_archivos()

#%%
''' Cargar datos a memoria'''
ld_c.percent = .001
for x in range (len(ld_c.lst_files)):
    ld_c.get_data_csv(ld_c.lst_files[x])
    #valida nulos
    ld_c.limpia_data()
    print(ld_c.data)
    df = ld_c.data
    profile = ProfileReport(df, 
                            title="Pandas Profiling Report", 
                            explorative=True,
                            minimal=True)

    profile.to_file(ld_c.lst_files[x]+"_pandas_profiling_report"+str(x)+".html")
