from tkinter import Tk, Label, Button

class archivoMain:
    
    def menuPrincipal(self):
        print(chr(142)*40)
        print(chr(142)*40)
        print("Maestria Analitica de Datos".center(78))
        print("Big Data".center(78))
        print("Proyecto Final".center(78))
        print("Arnulfo Garzon, Allan Veloza, Fabian Fuertes".center(78))
        print(chr(142)*40)
        print("Menu Principal".center(78))
        print("Opcion 1: Descargar datos Kaggle      ".center(78))
        print("Opcion 2: Descargar datos Twiter      ".center(78))
        print("Opcion 3: Limpiar datos Kaggle        ".center(78))
        print("Opcion 4: Limpiar datos Twitter y A.S.".center(78))
        print("Opcion 5: Salir                       ".center(78))
        print(chr(142)*40)
        
    def salir(self):
        print(chr(142)*40)
        print("Opcion 6. Salir".center(78))
        print("Gracias..............".center(78))
 
    def importKaggle(self):
        print(chr(142)*40)
        print("Opcion 1: Descargar datos Kaggle  ".center(78))
        try:
            import mainKaggle
        except Exception as e:
            print(e)
            quit
            
    def importTweet(self):
        print(chr(142)*40)
        print("Opcion 2: Descargar datos Twiter  ".center(78))
        try:
            import main_tweets
        except Exception as e:
            print(e)
            quit
            
    def cleanKaggle(self):
        print(chr(142)*40)
        print("Opcion 3: Limpiar datos Kaggle    ".center(78))
        try:
            print("espacio para funcion")
        except Exception as e:
            print(e)
            quit
            
    def cleanTweet(self):
        print(chr(142)*40)
        print("Opcion 4: Limpiar datos Twitter   ".center(78))
        try:
            import cleanTwitt
        except Exception as e:
            print(e)
            quit
            
            
#%%
proce = archivoMain()
proce.menuPrincipal()
op = int(input('Ingrese opcion: '))
while(True):
    if op == 1:
        proce.importKaggle()
        break
    elif op == 2:
        proce.importTweet()
        break
    elif op == 3:
        proce.cleanKaggle()
        break
    elif op == 4:
        proce.cleanTweet()
        break
    elif op == 5:
        proce.salir()
        break
        
