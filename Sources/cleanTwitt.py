# -*- coding: utf-8 -*-
"""
@author: Al amanecer me llamo Khepri, al mediodía Ra y al atardecer Atum.
@autor: Fabian Fuertes
@Autor Allan Veloza
@Autor Arnulfo Garzon


"""
try:
    import os, sys, inspect
    from pathlib import Path as p
    import time

except Exception as exc:
            print('Module(s) {} are missing.:'.format(str(exc)))

cmd_folder_b = os.path.realpath(
    os.path.abspath(os.path.split(
        inspect.getfile( inspect.currentframe()))[0]))

cmd_folder = str(p(cmd_folder_b) /'clases')
if cmd_folder not in sys.path:sys.path.insert(0, cmd_folder)

from cls_clean_tweets import CleanTwitts as data_T_clean


print("Leyendo dataset de twitter")
dataPath = os.path.abspath(os.path.join('__file__', '..','..'))+"\Dataset"

i_clean = data_T_clean(dataPath)
print ("Start : %s" % time.ctime())
i_clean.get_lst_files()
i_clean.get_data_csv()
i_clean.removeDuplicates()
i_clean.remove_URLsRTsandTwitterHandles()
i_clean.dateFOrmat()
i_clean.wordcloud('tweet_text')
#%%
i_clean.analisiSentiment()
print ("End : %s" % time.ctime())
