# -*- coding: utf-8 -*-
__author__ = "Allan Veloza, Arnulfo Garzon, Fabian Fuertes"
__maintainer__ = "Asignatura Big Data - ETL"
__copyright__ = "Copyright 2021 - Asignatura Big Data"
__version__ = "0.0.1"
# Antes de usar asegurese que tenga wordcloud:
# pip install wordcloud
# pip install nltk
# pip install textblob


try:
    import re
    import pandas as pd
    from wordcloud import WordCloud, STOPWORDS
    import matplotlib.pyplot as plt
    import nltk
    import textblob
    nltk.download('vader_lexicon')
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    from textblob import TextBlob
    import seaborn as sns
    import glob
    
    
except Exception as exc:
            print('Module(s) {} are missing.:'.format(str(exc)))

class CleanTwitts(object):
    
    def __init__(self, path=None):
        self.path = path
        self.data = None
        self.lst_files = None
        
    def get_data_csv(self):

        try:
            for i in self.lst_files:
                dataset = pd.read_csv(i, skip_blank_lines = True)
                self.data = pd.concat([self.data , dataset])
                print(self.data.shape)
        except Exception as exc:
            print(exc)
        
    def removeDuplicates(self):
        self.data = self.data.drop_duplicates(subset=['id'], keep='first')
        
        
    def remove_URLsRTsandTwitterHandles(self):

        try:
            for i in range(len(self.data['tweet_text'])):
                self.data['tweet_text'][i] = " ".join([word for word in self.data['tweet_text'][i].split()
                                            if 'http' not in word and '@' not in word and '<' not in word])    
                
            self.data['tweet_text'] = self.data['tweet_text'].apply(lambda x: re.sub('[(!@/#$:).;,?&]', '', x.lower()))
            self.data['tweet_text'] = self.data['tweet_text'].apply(lambda x: re.sub('  ', ' ', x))
        except Exception as exc:
            print(exc)
        
    def dateFOrmat(self):
        self.data['date'] = pd.to_datetime(self.data['date'],format ='%Y-%m-%d %H:%M:%S').dt.strftime('%d-%m-%Y')
        
    def wordcloud(self,col):
        stopwords = set(STOPWORDS)
        wordcloud = WordCloud(background_color="white",stopwords=stopwords,random_state = 2016).generate(" ".join([i for i in self.data[col]]))
        plt.figure( figsize=(20,10), facecolor='k')
        plt.imshow(wordcloud)
        plt.axis("off")
        plt.title("Buenos Días Ciencia")
        
    def analisiSentiment(self):
        z = 'ti was a son of a bitch :) :)'
        sid = SentimentIntensityAnalyzer()
        resultados = sid.polarity_scores(z)
        print(resultados)
        
        #self.get_data_csv()
        
        sid = SentimentIntensityAnalyzer()
        self.data['sentimiento'] = self.data['tweet_text'].apply(lambda i: sid.polarity_scores(str(i))['compound'])
        
        self.data['polaridad'] = self.data['tweet_text'].apply(lambda x: TextBlob(x).sentiment.polarity)
        self.data['subjetividad'] = self.data['tweet_text'].apply(lambda x: TextBlob(x).sentiment.subjectivity)
        
        sns.set(rc={'figure.figsize':(15,15)})
        sns.distplot(self.data['polaridad'])
        sns.distplot(self.data['subjetividad'])
        sns.distplot(self.data['sentimiento'])
        
    def get_lst_files(self,tipo='csv'):
        '''
        Lista los archivo de un directorio segun el tipo de solicitado.

        Parameters
        ----------
        tipo : string
            Extensión o tipo de archivo.

        Returns
        -------
        None.

        '''
        try:
            self.lst_files = [f for f in glob.glob(str(self.path)+'/**/*.'+ tipo.lower(), recursive=True)]
            
        except Exception as exc:
            self.show_error(exc)
        
    