# -*- coding: utf-8 -*-
__author__ = "Allan Veloza, Arnulfo Garzon, Fabian Fuertes"
__maintainer__ = "Asignatura Big Data - ETL"
__copyright__ = "Copyright 2021 - Asignatura Big Data"
__version__ = "0.0.1"


try:
    from pathlib import Path as p
    import pandas as pd
    import glob
    import os
    import statistics as stats

    import shutil
    import chardet

    import sys
    
except Exception as exc:
            print('Module(s) {} are missing.:'.format(str(exc)))

#%%
class carga_data_mf(object):
    
    
    def __init__(self, path=None,percent=None):
        self.path = path
        self.percent = percent
        self.data = None
        self.lst_files = None
        self.competition = None
        self.dataset = None
        self.status = False
        self.lst_files_d = None


    def set_kaggle_api(self):
        '''
        Validar acceso a los datos.

        Returns
        -------
        None.

        '''
        try:
            mpath = os.path.abspath(os.path.join(__file__, '..','..','..'))
            path_kaggle = str(p(mpath) / 'kaggle')
            os.environ['KAGGLE_CONFIG_DIR'] = path_kaggle
            
            from kaggle.api.kaggle_api_extended import KaggleApi
            self.api = KaggleApi()
            self.api.authenticate()
            self.status = True
            
        except Exception as exc:
            
            self.show_error(exc)
            
    def check_used_space(self,path):
        try:
            self.check_path(path)
            if self.dir_exist:
                total_size = 0
                
                #use the walk() method to navigate through directory tree
                for dirpath, dirnames, filenames in os.walk(path):
                    for i in filenames:
                        
                        #use join to concatenate all the components of path
                        f = os.path.join(dirpath, i)
                        
                        #use getsize to generate size in bytes and add it to the total size
                        total_size += os.path.getsize(f)
                
                self.bytes = total_size
                total_size = self.formatSize()
                print('Espacio usado por el destino: {}'.format(total_size))
                return total_size
            else:
                print('{}No es posible calcular el espacio utilizado.'.format(os.linesep))
                print('El directorio {} no existe.'.format(path))
        
        except Exception as exc:
            self.show_error(exc)
        
            
    def check_free_space(self,path_data):
        try:
            self.bytes = shutil.disk_usage(str(path_data))[2]
            free_space = self.formatSize()
            print('Espacio libre en disco: {}'.format(free_space))
            return free_space
        
        except Exception as exc:
            self.show_error(exc)
        
    def formatSize(self):
        try:
            bytes = float(self.bytes)
            kb = bytes / 1024
        except:
            return "Error"
        if kb >= 1024:
            M = kb / 1024
            if M >= 1024:
                G = M / 1024
                return "%.2fG" % (G)
            else:
                return "%.2fM" % (M)
        elif kb == 0:
            return 'Folder vacio'
        else:
            return "%.2fkb" % (kb)

    def check_path(self,path_check):
        '''
        Valida que exista el path

        Returns
        -------
        None.

        '''
        self.dir_exist = os.path.exists(path_check)
        
    def get_lst_files(self,path_data,tipo):
        '''
        Lista los archivo de un directorio segun el tipo de solicitado.

        Parameters
        ----------
        path_data : string
            Ruta del directorio que contiene los archivos.
        tipo : string
            Extensión o tipo de archivo.

        Returns
        -------
        None.

        '''
        try:
            self.lst_files = [f for f in glob.glob(str(path_data)+'/**/*.'+ tipo.lower(), recursive=True)]
            
        except Exception as exc:
            self.show_error(exc)
            
    def determina_tipo_archivo(self,Ruta):
        '''
        Borra lo que no esta permitido
        
        Parameters
        ----------
        Ruta : String
            Ruta donde estan los archivos
    
        Returns
        -------
         Imprime si el archivo esta permitido, en caso de que no 
         lo borra de la ruta.
         '''
        archivosRuta = os.listdir(Ruta)
        for archivo in archivosRuta:
            print("Esta validando la ext del archivo: " + archivo)
            borra = archivo
            archivo = archivo.lower()
            if archivo.endswith('.csv') or archivo.endswith('.json') or archivo.endswith('.xml'):
                print("Tipo archivo ok")
            else:
                print("Tipo archivo no permitido")
                os.remove(Ruta +"/"+ borra)
                
    def describe_data(self,ds):
        '''
        Listamos el tipo de la variable
        
        Parameters
        ----------
        ds : Dataframe
            archivo en tipo dataframe
    
        Returns
        -------
         Imprime las columnas, el tipo, cuantos datos unicos hay y la cantidad de nulos.
         '''
        print ('-'*60)
        print ('Columnas \t\tTipo \t\tUnicos \t\tNulos')
        print ('-'*60)
        for i in ds.columns.tolist():
            print ('%s: \t\t%s \t\t%d/%d, \t\t%d'%(i, ds[i].dtype,len(ds[i].unique()),len(ds[i]),len(ds[ds[i].isnull()])))
        print ('-'*60)
        print ('-'*60)
        print ('\n')
        
    def lista_variables_tipo(self,df):
        '''
        Listamos el tipo de la variable
        
        Parameters
        ----------
        ds : Dataframe
            archivo en tipo dataframe
    
        Returns
        -------
         Imprime las columnas, el tipo.
         
         '''
        print ('-'*60)
        print ('Columnas \t\tTipo')
        print ('-'*60)
        print(df.dtypes)
        print ('-'*60)
        print ('-'*60)
        print ('\n')
    def distribucion_variable(self,df):
        '''
        verifica la distribucion de la variable
        
        Parameters
        ----------
        df : Dataframe
            archivo en tipo dataframe
    
        Returns
        -------
         Muestra Media, Mediana y Moda para variables cuantitativas
         Muestra los valores unicos de variables cualitativas.
         '''
        cuantitativas = df.select_dtypes(exclude=["object"])
        if('CEDueDate' in cuantitativas):
            cuantitativas = cuantitativas.drop(['CEDueDate'], axis = 1)
        cuali = df.select_dtypes(include=["object"])
        print ('-'*60)
        print("Cualitativas:")
        print ('-'*60)
        for i in cuali:
            print ('%s:  %a'%(i, cuali[i].unique()))
            print('\n')
        print ('-'*60)
        print ('\n')
        print("Cuantitativas:")
        print ('-'*60)
        for i in cuantitativas:
            print(i)
            print('Media   : %f'%(stats.mean(cuantitativas[i])))
            print('Mediana : %i'%(stats.median(cuantitativas[i]))) 
            print('Moda    : %i'%(stats.mode(cuantitativas[i])))
            print('\n')
        print ('-'*60)
        print ('-'*60)
        print ('\n')
    # Carga datos desde archivos tipo xml
    def get_data_csv(self, the_path):
        '''
        Parameters
        ----------
        the_path : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''

        try:
            self.data = pd.read_csv(the_path, skip_blank_lines = True)
            
        except Exception as exc:
            self.show_error(exc)
      

    
    def print_nested_keys(self,dic,path=''):
        for k,v in dic.items():
            if isinstance(v,dict):
                path+=k+"."
                yield from self.print_nested_keys(v,path)
            else:
                path+=k
                yield path
                
    def json_dict_to_df(self,vKey = None):
        try:
            if vKey is not None:
                mydata = self.data[vKey]
            else:
                mydata = self.data
                
            df = pd.DataFrame(mydata)
            
            print(os.linesep)
            print('{}{}{}'.format(os.linesep,df.info(),os.linesep))
            print(df)
            
        except Exception as exc:
            self.show_error(exc)
			
    def get_data_csv_nozip(self, the_path):
        '''
        Parameters
        ----------
        the_path : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''

        try:
            with open(the_path, 'rb') as fx:
                result = chardet.detect(fx.read())
                
            child = os.path.splitext(os.path.basename(the_path))[0]
            
            print('File: {} - {}'.format(child,result))
            
            self.data = pd.read_csv(the_path,
                                    encoding = result['encoding'],
                                    nrows = 100
                             )

        except Exception as exc:
            self.show_error(exc)
                  
        
    # Control de exceptions
    def show_error(self,ex):
        '''
        Captura el tipo de error, su description y localización.

        Parameters
        ----------
        ex : Object
            Exception generada por el sistema.

        Returns
        -------
        None.

        '''
        trace = []
        tb = ex.__traceback__
        while tb is not None:
            trace.append({
                          "filename": tb.tb_frame.f_code.co_filename,
                          "name": tb.tb_frame.f_code.co_name,
                          "lineno": tb.tb_lineno
                          })
            
            tb = tb.tb_next
            
        print('{}Something went wrong:'.format(os.linesep))
        print('---type:{}'.format(str(type(ex).__name__)))
        print('---message:{}'.format(str(type(ex))))
        print('---trace:{}'.format(str(trace)))
        self.status = False
        
    # Varios
    def muestra_data(self,df,cant):
        '''
        Muestra el contenido de un dataframe, 
        dependiendo de la cantidad de registros que desea mostrar
        
        Parameters
        ----------
        df : DataFrame
        
        cant: int
            valor de la cantidad de registros que quiere mostrar

        Returns
        -------
        None.
        '''
        # OpciÃ³n de mostar X cantidad de registros o la totalidad
        pd.set_option('display.max_rows',None)
        pd.set_option('display.max_columns',None)
        pd.set_option('display.width',None)
        pd.set_option('display.max_colwidth', -1)
        print(df.head(cant))
        
    def muestra_archivos(self):
        '''
        Imprime en pantalla cada uno de los elementos contenidos en lst_files

        Returns
        -------
        None.

        '''
        try:
            for f in self.lst_files:
                child = os.path.splitext(os.path.basename(f))[0]
                print(child)
                
        except Exception as exc:
            self.show_error(exc)
            
            
    def save_df(self,df,filename = None):
        try:
            if filename is not None:
               df.to_excel(filename , sheet_name = 'sheet', index=False)
            
        except Exception as exc:
            self.show_error(exc)
    
    def get_slide(self,df):
        '''
        Toma X porcentaje de registros de un conjunto de datos.

        Parameters
        ----------
        df : Pandas DataFrame

        Returns
        -------
        df : Pandas DataFrame

        '''
        try:
            print('There are {} register(s) in the dataset.'.format(df.shape[0]))
            if 0.01 <= self.percent <= 0.99:
                end = int(len(df)*self.percent)
                return df.iloc[:end]
            else:
                return df
            
        except Exception as exc:
            self.show_error(exc)
            
    def limpia_data(self):
        self.data.drop_duplicates(keep=False,inplace=True)
        
    def control_error(self,mensaje,flagError):
        '''
        toma el control de la ejecucion del programa
        si este contiene errores no continua y muestra el error de 
        la validacion

        Parameters
        ----------
        mensaje : String, almacena el mensaje de error
        flagError : Bolean, True si contiene error, False si no tiene error.

        Returns
        -------
        none

        '''
        if(flagError):
            print(mensaje)
            #quit()
            sys.exit()
        
    #datasets kaggle
    def get_dataset_kaggle(self):
        '''
        Descarga el dataset de kaggle

        Parameters
        ----------
        none

        Returns
        -------
        none

        '''
        self.api.dataset_download_files(self.competition,
                           path = self.path, unzip=True)
        
    def list_dataset_kaggle(self):
        '''
        Lista los Datasets (no son competencias) de kaggle

        Parameters
        ----------
        none

        Returns
        -------
        none

        '''
        self.lst_files_d = self.api.datasets_list(search=self.competition)
        print('{}Datasets in: {}'.format(os.linesep,self.competition))
        print(self.lst_files_d[0]['ref'])
        
        
    def datasets_list_files(self):
        '''
        Lista los archivos contenidos en el daset

        Parameters
        ----------
        none

        Returns
        -------
        none

        '''
        try:
            self.lst_files_d = self.api.datasets_list_files('mkechinov','ecommerce-behavior-data-from-multi-category-store')
            print('{}Datasets in: {}'.format(os.linesep,self.competition))
            Listado = self.lst_files_d['datasetFiles'][0]
            print(Listado['name'])
        
        except Exception as exc:
            self.show_error(exc)
        
    