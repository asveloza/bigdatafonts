# -*- coding: utf-8 -*-
__author__ = "Allan Veloza, Arnulfo Garzon, Fabian Fuertes"
__maintainer__ = "Asignatura Big Data - ETL"
__copyright__ = "Copyright 2021 - Asignatura Big Data"
__version__ = "0.0.1"


try:
    #from pathlib import Path as p
    import pandas as pd
    #import tweepy as tp
    import json
    import  os
    import tweepy
    #python -m pip install pymongo[srv]
    import pymongo
    
    from pymongo import MongoClient


except Exception as exc:
            print('Module(s) {} are missing.:'.format(str(exc)))
            

class extrae_tweets(object):
    
    def __init__(self, path=None,percent=None):
        self.df = None
        self.client = None
        self.filepath = 'C:\\Users\\allan\\Documents\\bigdatafonts\\Dataset\\df_search1.csv'
        

    
    def searchTweets(self,query, lang, cant):#, geo):
        '''
         Perform a query search request to Twitter with Tweepy. Parameters:
         - query: include a word or a complex query using operators
         - lang: language ISO code 639-1
         - geo: latitude, longitude and km or miles
        
         Returns a dataframe with the columns: query, date, id, user and text.
     
        '''
        consumer_key = 'U9YPsRRXMZwQHggLH9td1cTAc'
        consumer_secret = 'Or9HkGAlQlGt46wHUN02DG5GzQrNUEZNQglBMQBGsj0fGgSA5t'
        
        call_back_url='oob'
        auth=tweepy.OAuthHandler(consumer_key,consumer_secret,call_back_url)
        
        api=tweepy.API(auth)
        
        tweets = tweepy.Cursor(api.search,
                           q=query,
                           lang=lang,
                           #geo=geo,
                           result_type='mixed').items(cant)
    
    
        data = [[query,tweet.created_at, tweet.id, tweet.user.screen_name, tweet.text] for tweet in tweets]
    
        df_tw = pd.DataFrame(data=data, columns=['query','date', 'id', 'user', 'tweet_text'])
        print('Here you have your first 10 tweets')
        print(df_tw)
    
        #count += 1
        self.df=df_tw
        
        
        df_tw.to_csv(self.filepath)  

        


    def carga_mongo (self,df):    
        mng_client = pymongo.MongoClient('mongodb+srv://ffuertesg:Colombia01*@cluster0.w3dyb.mongodb.net/ffuertesg?retryWrites=true&w=majority')
        mng_db = mng_client['Bigdata01'] # Remplace nombre mongo db
        collection_name = 'tweets' #// Remplace coleccion mongo db 
        db_cm = mng_db[collection_name]
        records=db_cm.tweets
        print(records.count_documents({}))
        
        
        try :
            #child = os.path.splitext(os.path.basename('df_search.csv'))[0]
            data = df
            data_json = json.loads(data.to_json(orient='records'))
            #db_cm.remove()
            #print(data_json)
            db_cm.insert(data_json)
        except :
            print('Error al cargar el archivo')
              


